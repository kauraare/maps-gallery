# Places
Simple webserver to display images with geotags on google maps as clusters and view them in gallery mode.

### Usage
Run `scripts/resizeimages.sh` to resize images and generate thumbnails in directories `images` and `thumbs` respectively

### Example
A working example of such webserver is hosted on https://photos.kaur.eu/
