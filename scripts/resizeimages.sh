#!/bin/bash
if [ "$#" -ne 2 ]; then
    echo "Usage: resizeimages.sh [source] [destination]" && exit -1
fi

SRC="$1"
DST="$2"

## make thumbnails and previews
while read type res qual; do
    echo Generating $type
    find "$SRC" -name highlights -prune -o -type d -printf "$DST/$type/%P\0" | xargs -0 mkdir -p
    comm -23 \
         <(find "$SRC"       -name '*.jpg' -printf '%P\n' | sort) \
         <(find "$DST/$type" -name '*.jpg' -printf '%P\n' | sort) |
            grep -v '^highlights' |
            xargs -I@ -P $(nproc) convert $SRC/@ -geometry $res -quality $qual $DST/$type/@
done <<EOF
thumbs  360x 75
images x2160 80
EOF

## read exif data
echo Generating exif database
DBFILE="${DST}/data/imgdata.csv"

comm -23 \
     <(find "$SRC" -name 'highlights' -prune -o -type f -printf '%P\n' | sort) \
     <(cat "${DBFILE}" | cut -f1 -d, | tail -n+2 | sort) |
    xargs -I@ echo ~/photos/albums/@ |
    xargs exiftool -csv -r -c "%+.6f" \
          -datetimeoriginal\
          -GPSLatitude \
          -GPSLongitude \
          -make \
          -model \
          -exposuretime \
          -iso \
          -fnumber \
          -lensmodel \
          -imagesize \
          -focallength |
    sed "s|$SRC/||g" > "${DBFILE}.new"

echo "SourceFile,DateTimeOriginal,GPSLatitude,GPSLongitude,Make,Model,ExposureTime,ISO,FNumber,LensModel,ImageSize,FocalLength" > "${DBFILE}.merged"

cat <(tail -n+2 "$DBFILE") \
    <(tail -n+2 "${DBFILE}.new") |
    sort -rk2 -t, >> "${DBFILE}.merged"

rm "${DBFILE}.new"
mv "${DBFILE}.merged" "$DBFILE"

echo 'Removing EXIF data from thumbs for performance'
exiftool -r -overwrite_original -all= "$DST/thumbs"
