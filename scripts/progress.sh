#!/bin/bash
cd "$(dirname "$0")/.."

if [ -d ~/photos/albums ]; then
    printf 'Total:   '
    find ~/photos/albums -name '*.jpg' -printf '%P\n' | grep -v highlights | wc -l
fi

printf 'Thumbs:  '
find thumbs -name '*.jpg' | wc -l

printf 'Images:  '
find images -name '*.jpg' | wc -l

echo

printf 'Thumbs:  '
du -sh thumbs | cut -f1

printf 'Images:  '
du -sh images | cut -f1
