var scsv = null;

$.ajax({
    async: false,
    global: false,
    url: "data/imgdata.csv",
    dataType: "text",
    success: function(data) {
        scsv = data;
    }
});

var AllImages  = $.csv.toObjects(scsv); // All images in the database
var ActiveImages = new Array(); // Images in current map view
var LatLngArray = new Array();

function initMap() {
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 3,
        center: {lat: 20, lng: 10},
        mapTypeId: "roadmap"
    });
    for (i = 0; i < AllImages.length; i++) {
        AllImages[i].Location = AllImages[i].GPSLatitude + ", " + AllImages[i].GPSLongitude;
        LatLngArray[i] = new google.maps.LatLng({
            lat: parseFloat(AllImages[i].GPSLatitude),
            lng: parseFloat(AllImages[i].GPSLongitude)
        });
        delete AllImages[i].GPSLongitude;
        delete AllImages[i].GPSLatitude;
    }
    markers = LatLngArray.map(function(location, i) {
        return new google.maps.Marker({
            position: location,
        });
    });

    var markerCluster = new MarkerClusterer(map, markers, {
        imagePath: "icons/m",
        minimumClusterSize: 1,
        gridSize: 30
    });

    // Update list
    map.addListener("center_changed", function() {UpdateActiveList()});
    map.addListener('idle',           function() {UpdateActiveList()});

    function UpdateActiveList() {
        bb = map.getBounds();
        document.getElementById("left").innerHTML = "";
        var counter = 0;
        ActiveImages = []
        for (var i = 0; i < LatLngArray.length; i++) {
            if (bb.contains(LatLngArray[i])) {
                ActiveImages[counter] = AllImages[i];
                var url = AllImages[i].SourceFile;
                var a = document.createElement("a");
                a.href = "images/" + url;
                // Show only first 100 in left pane for performance
                if (counter < 100) {
                    var img = document.createElement("img");
                    img.src = "thumbs/" + url;
                    a.appendChild(img);
                }
                document.getElementById("left").appendChild(a);
                counter = counter + 1;
            }
        }
    }
    
    $("#left").magnificPopup({
        delegate: "a",
        type: "image",
        showCloseBtn: false,
        gallery: {enabled: true}
    });
}
